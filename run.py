import argparse
import os
import utils
import time
import numpy as np


def localization_inference(filepath):
    # set paths
    img_out_dir = "img_check/pts_check"
    vessel_model_dir = "model/vessel"
    localization_model_dir = "model/od_fovea_localization"
    utils.make_new_dir(img_out_dir)
    
    # load the models and corresponding weights
    vessel_model = utils.load_network(vessel_model_dir)
    localization_model = utils.load_network(localization_model_dir)
    
    # iterate all images
    start_time = time.time()

    # load an img (tensor shape of [1,h,w,3])
    img = utils.imagefiles2arrs([filepath])
    resized_img, y_offset, x_offset, cropped_w = utils.resize_img(img, 640, 640)

    # run inference
    vessel = vessel_model.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
    predicted_coords, _ = localization_model.predict([utils.normalize(resized_img, "od_from_fundus_vessel"), (vessel * 255).astype(np.uint8) / 255.], batch_size=1)
    
    # convert to coordinates in the original image
    coords = utils.convert_coords_to_original_scale(predicted_coords, y_offset, x_offset, cropped_w)
    
    # save the result
    utils.save_imgs_with_pts(img.astype(np.uint8), coords.astype(np.uint16), img_out_dir, os.path.basename(filepath).replace(".jpg", ""))
    
    print "od: ({}, {})".format(coords[0, 1], coords[0, 0])
    print "fv: ({}, {})".format(coords[0, 3], coords[0, 2])
    print "duration: {} sec".format(time.time() - start_time)


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--filepath',
    type=str,
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# training settings 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# localization
localization_inference(FLAGS.filepath)
